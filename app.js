const express = require("express"),
app = express(),
path = require("path")

const PORT = process.env.APP_PORT || 3000 
// app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

app.get("*", (req, res) =>{
    res.render("index", {name: "Ali"})

})

app.listen(PORT, () =>{
    console.log(`server started at: http://localhost:${PORT}`);
})
